import React, { useState, useEffect } from 'react';
import { moviedB,wishdB } from '../Firebase';
import { ref, child, get } from "firebase/database";
import { doc, setDoc } from "firebase/firestore"; 
import { useNavigate } from 'react-router-dom';
import { Table } from 'reactstrap';
import { collection,getDocs } from 'firebase/firestore';



const Home = () => {
    
    const [data,setData] = useState({});  //data will contain all the movies
    const [movies,setMovies] = useState({});  //data will contain all the movies
    const [search, setSearch] = useState(""); //search what the user types in the search bar

    let navigate = useNavigate();
    const moviedBRef = ref(moviedB);
    let isMounted=false;
    //if the user types the name of the movie
    const handleSubmit= (e) => {
        e.preventDefault();
        navigate('/search?name='+search);
        setSearch("");
    }

    //add a movie to the wishlist
    const addWishlist = async (movie) => {
        try {
            await setDoc(doc(wishdB, localStorage.getItem("id"),movie.id.toString()), {
              id: movie.id,
              title: movie.title,
              year:movie.year,
              genre: movie.genre
            });
          } catch (e) {
            console.error("Error adding document: ", e);
          }}
    

    useEffect(() => {
        isMounted=false;
        get(child(moviedBRef,"movies-list")).then((snapshot) => {
            if (snapshot.exists()) {
                setData({...snapshot.val()});
            }
            else {
                setData({});
            }
        })
            return () => {
                setData({});
                isMounted=true;
        };
    },[]);

    useEffect(()=> {
        isMounted=false;
        const getMovies = async () => {
            const data = await getDocs(collection(wishdB,localStorage.getItem("id")));
            setMovies(data.docs.map((doc) => ({...doc.data(), id: doc.id})))
        };
        isMounted=true;
        getMovies();
    }, []);

    if(!isMounted) { 
        return (
        <div align="center">
            
            <h4> Search a movie:</h4>
            
            <form onSubmit={handleSubmit}>
                <input
                type="text"
                className='inputField'
                placeholder='Search...'
                onChange={(e) => {setSearch(e.target.value)}}
                value={search}
                />
            </form>

            {/* Displaying all the movies */}
            <Table responsive striped bordered hover>
                <thead>
                    <tr>
                        <th>Title</th>
                        <th>Year</th>
                        <th>Genre</th>
                        <th>Add to Wishlist </th>
                    </tr>
                </thead>
                <tbody>
                   {Object.keys(data).map((index) => {
                            return (
                                <tr>
                                    <td>{data[index].title}</td>
                                    <td>{data[index].year}</td>
                                    <td>{data[index].genre}</td>
                                    <td>
                                        <button onClick={() => {addWishlist(data[index])}}> <img src={require('../heart.jpg')} alt='' height={20} width={20} /></button>
                                    </td>
                                </tr>
                            )   
                        })
                       }
                </tbody>

            </Table>
        </div>
    );}
    else { //if the wishlist is empty
        return(
            <div align="center">
                <p> Loading...</p>
            </div>
            
        )
    }
    
}
export default Home;