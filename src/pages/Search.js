import {doc,setDoc,} from 'firebase/firestore';
import React, { useState, useEffect } from 'react';
import { useLocation} from 'react-router-dom';
import { wishdB} from '../Firebase';
import { Table } from 'reactstrap';
import { getDatabase, ref,onChildAdded} from "firebase/database";


const Search = () => {
    
    const [data, setData] = useState([]); //data will contain information regarding the searched movies

    const useQuery = () => {
        return new URLSearchParams(useLocation().search);
    }

    let userQuery = useQuery();
    let search = userQuery.get("name"); //in case the user wrote in the search bar "Name"
    //add the search movie to the wishlist
    const addWishlist = async (movie) => {
        try {
            await setDoc(doc(wishdB, localStorage.getItem("id"),movie.id.toString()), {
              id: movie.id,
              title: movie.title,
              year:movie.year,
              genre: movie.genre
            });
          } catch (e) {
            console.error("Error adding document: ", e);
          }}
    useEffect(() => {
        searchData();
    }, [search])

    
    const searchData = () => {
        //search a movie by its name
        const db = getDatabase();
        const titleRef = ref(db, 'movies-list')
        const dataGood=[];
        onChildAdded(titleRef, (dataSearched) => {
            const titleSearching=dataSearched.val().title;
            const yearSearching=dataSearched.val().year;
            const genreSearching=dataSearched.val().genre;
            if(titleSearching.toLowerCase().search(search.toLowerCase())!==-1) {
            dataGood.push(dataSearched.val());
            }
            if(yearSearching===parseInt(search)) {
            dataGood.push(dataSearched.val());
            }
            if(genreSearching.toLowerCase()===search.toLowerCase()) {
                dataGood.push(dataSearched.val());
                }}
        )
        setData(dataGood);
    }


    return (
       
        <div align="center">
             {/* Displaying the movies that match what the user searched */}
            <Table responsive bordered striped hover>
                <thead>
                    <tr>
                        <th>Title</th>
                        <th>Year</th>
                        <th>Genre</th>
                        <th>Add to Wishlist </th>
                    </tr>
                </thead>
                <tbody>
                   {Object.keys(data).map((index) => {
                       return (
                           <tr key={data[index].id}>
                               <td>{data[index].title}</td>
                               <td>{data[index].year}</td>
                               <td>{data[index].genre}</td>
                               <td>
                                   <button onClick={() => {addWishlist(data[index])}}><img src={require('../heart.jpg')} height={20} width={20} /></button>
                               </td>
                           </tr>
                       )
                   })} 
                </tbody>

            </Table>
        </div>
    );
}

export default Search;