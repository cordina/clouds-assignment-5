import React from "react";
import { auth, provider } from "../Firebase";
import { signInWithPopup } from 'firebase/auth';
import { useNavigate } from "react-router-dom";

function Login({ setIsAuth }) {
    
    let navigate = useNavigate();

    //login function
    const signInWithGoogle = () => {
        signInWithPopup(auth,provider)
        .then((result) => {
            localStorage.setItem("name",result.user.displayName);
            localStorage.setItem("id", result.user.uid);
            localStorage.setItem("isAuth", true);
            setIsAuth(true);
            navigate('/');
        })
        .catch((error) => {
            console.log(error);
        });
    }

    return (

        <div className="loginPage" align="center">
            <br/>
            <img src={require('../logo.jpg')} alt='' height={200} width={200}/>
            <br/>
            <button onClick={signInWithGoogle}> Sign in with Google </button>
            </div>
    );
}
export default Login;