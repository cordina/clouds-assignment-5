import React, { useState, useEffect } from 'react';
import { wishdB} from "../Firebase";
import { collection,getDocs } from 'firebase/firestore';
import { doc, deleteDoc } from 'firebase/firestore';
import { useNavigate } from "react-router-dom";
import { Link } from 'react-router-dom';
import { Table } from 'reactstrap';

function Wishlist() {
    const [movies, setMovies] = useState([]); //movies will contain movies that are in the user's wishlist

    let navigate = useNavigate();

    //delete function
     const deleteWishlist = async (movie) => {
        await deleteDoc(doc(wishdB, localStorage.getItem("id"), movie.id));
        navigate('../');
        navigate('/wishlist'); 
    }

    useEffect(()=> {
        const getMovies = async () => {
            const data = await getDocs(collection(wishdB,localStorage.getItem("id")));
            setMovies(data.docs.map((doc) => ({...doc.data(), id: doc.id})))
        };
        getMovies();
    }, []);
    
    if (movies.length!==0) { //if the wishlist is not empty: 
        return (
        <div align="center">
            {/* Displaying the movies in the wishlist */}
            <Table responsive striped bordered hover>
                <thead>
                    <tr>
                        <th>Title</th>
                        <th>Year</th>
                        <th>Genre</th>
                        <th>Remove from Wishlist </th>
                    </tr>
                </thead>
                <tbody>
                   {Object.keys(movies).map((index) => {
                       return(
                       <tr key={movies[index].id}>
                           <td>{movies[index].title}</td>
                            <td>{movies[index].year}</td>
                            <td>{movies[index].genre}</td>  
                            <td>
                                <button onClick={() => deleteWishlist(movies[index])}> <img src={require('../brokenheart.png')} alt='' height={20} width={20} /></button>
                            </td>
                        </tr>
                   )})}
                </tbody>
            </Table>
        </div>
    );}
    else { //if the wishlist is empty
        return(
            <div align="center">
                <p> You did not add any movie to your wishlist!</p>
                <Link to="../"> <button>Movies List</button></Link>
            </div>
            
        )
    }
    
}

export default Wishlist;
