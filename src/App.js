import { React,useState } from 'react';
import './App.css';
import { signOut } from 'firebase/auth';
import { BrowserRouter as Router, Routes, Route, Link } from 'react-router-dom';
import Home from './pages/Home';
import Login from './pages/Login';
import Search from './pages/Search';
import Wishlist from './pages/Wishlist';
import { auth } from './Firebase';

function App() {
  
  const [isAuth,setIsAuth] = useState(false);  //isAuth says whether an user is authentified or not

  //logout function
  const signUserOut = () =>{
    signOut(auth).then(() => {
      localStorage.clear();
      setIsAuth(false);
      window.location.pathname="/login";
    });
  }
  
  return (
    <Router>
      <nav className="navbar navbar-inverse">
        {/*Navigation Menu */}
        <div className="container-fluid">
        <Link to="/"> <img src="./logo.jpg"width={50} height={50}/></Link>
        <ul className="nav navbar-nav navbar-right">
        {isAuth && <button className="button">{localStorage.getItem("name")}</button>}
        {isAuth && <Link to="/wishlist"><img src={require("./heart.jpg")} width={50} height={50}/></Link>}
        {!isAuth ? <Link to="/login"> <button className="button"> Login </button></Link> : <button onClick={signUserOut} className="button"> Logout </button>}</ul></div>
      </nav>
      <div>
        {/* Welcoming message when the user is not logged in yet */}
        {!isAuth && (
          <div align="center">
            <h1> Movies Room </h1>
            <img src={require('./logo.jpg')} height={300} width={300}/>
            <p> 
              WELCOME ! 
              Please click on "Login" to continue.
            </p>
          </div>

        )}
      </div>

      <Routes>
        {isAuth && <Route path="/search" element={<Search />}/>}
        {isAuth && <Route path="/" element={<Home />} />}
        <Route path="/login" element={<Login setIsAuth={setIsAuth} />} />
        {isAuth && <Route path="/wishlist" element={<Wishlist />} />}
      </Routes>
    </Router>
    
  );
}

export default App;