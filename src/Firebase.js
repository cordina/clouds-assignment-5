// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAuth, GoogleAuthProvider } from 'firebase/auth'
import { getFirestore } from "firebase/firestore";
import firebase from "firebase/compat/app"
import { getDatabase } from "firebase/database";

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyCdxZjfZ5BBifGN0K3hYEHrLGVTTcgnAFg",
  authDomain: "cloud5-a61e0.firebaseapp.com",
  databaseURL: "https://cloud5-a61e0-default-rtdb.firebaseio.com",
  projectId: "cloud5-a61e0",
  storageBucket: "cloud5-a61e0.appspot.com",
  messagingSenderId: "102820510147",
  appId: "1:102820510147:web:5d30ccfaca0f33e24e6576",
  measurementId: "G-PFFSZ0JJX2"
};

// Initialize Firebase
export const fireDb = firebase.initializeApp(firebaseConfig);
export const app = initializeApp(firebaseConfig);
export const wishdB = getFirestore();
export const auth = getAuth(app);
export const provider = new GoogleAuthProvider(); 
export const moviedB = getDatabase();

